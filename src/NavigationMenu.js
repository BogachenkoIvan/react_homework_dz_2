import React, { useState } from 'react';
import { ReactComponent as SearchIcon} from '../src/menu-svg/search.svg';
import { ReactComponent as DashboardIcon } from '../src/menu-svg/dashboard.svg';
import { ReactComponent as RevenueIcon } from '../src/menu-svg/revenue.svg';
import { ReactComponent as NotificationsIcon } from '../src/menu-svg/notifications.svg';
import { ReactComponent as AnalyticsIcon} from '../src/menu-svg/analytics.svg';
import { ReactComponent as InventoryIcon} from '../src/menu-svg/inventory.svg';
import { ReactComponent as LogoutIcon} from '../src/menu-svg/logout.svg';
import { ReactComponent as LightModeIcon} from '../src/menu-svg/lightmode.svg';
import radioButtonIcon from '../src/menu-svg/radiobutton.svg';

const NavigationMenu = () => {
const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  return (
    <div
      className={`menu-container ${isHovered ? 'hovered' : ''}`}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className="menu-header">
        <div className={`avatar ${isHovered ? 'hovered' : ''}`}>
          <p>AF</p>
        </div>
        {isHovered && (
          <div className={`user-info ${isHovered ? 'visible' : ''}`}>
            <h2>AnimatedFred</h2>
            <h3>animated@demo.com</h3>
          </div>
        )}
      </div>
      
        <>
            <div className="search">
                <SearchIcon />
                {isHovered && <input type="text" placeholder="Search..." />}
            </div>
            <div className="menu-items">
            <a href="/dashboard" className="menu-item">
                <DashboardIcon />
                {isHovered && <span>Dashboard</span>}
            </a>
            <a href="/revenue" className="menu-item">
                <RevenueIcon />
                {isHovered && <span>Revenue</span>}
            </a>
            <a href="/notifications" className="menu-item">
                <NotificationsIcon /> 
                {isHovered && <span>Notifications</span>}
            </a>
            <a href="/analytics" className="menu-item">
                <AnalyticsIcon />
                {isHovered && <span>Analytics</span>}
            </a>
            <a href="/inventory" className="menu-item">
                <InventoryIcon />
                {isHovered && <span>Inventory</span>}
            </a>
            </div>
            <div className="space"></div>
            <a href="/logout" className="menu-item">
                <LogoutIcon />
                {isHovered && <span>Logout</span>}
            </a>
            <label className="menu-item">
                {isHovered &&<LightModeIcon />}
                {isHovered && <span className="lightspan">Light mode</span>}
                <span className="radio-icon">
                    <input type="radio" className="radio-input" />
                    <img src={radioButtonIcon} alt="RadioButton" />
                </span>
            </label>
        </>
     
    </div>
  );
};

export default NavigationMenu;