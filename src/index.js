import React from 'react';
import { createRoot } from 'react-dom/client';
import NavigationMenu from './NavigationMenu';


const root = document.getElementById('main');

createRoot(root).render(<NavigationMenu></NavigationMenu>);